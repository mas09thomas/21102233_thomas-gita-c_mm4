import cv2
import numpy as np
from matplotlib import pyplot as plt

imggambar = cv2.imread("soal2.png")
kernel = np.ones((1,1),np.uint8)
kernel2 = np.ones((15,15),np.uint8)

imgCanny = cv2.Canny(imggambar,10,150)
imgdilation = cv2.dilate(imgCanny,kernel,iterations=1)
imgdilation2 = cv2.dilate(imgCanny,kernel2,iterations=1)
imgdilation3 = imgdilation2

tampil_hor=np.concatenate((imggambar,imgdilation2),axis=0)

# Tambahkan label di kiri atas dengan teks hitam dan font yang lebih kecil
text = "21102233_Thomas Gita Christiandaru"
cv2.putText(imggambar, text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 0), 1)

cv2.imshow('hasil akhir',tampil_hor)

cv2.waitKey(0)
cv2.destroyAllWindows