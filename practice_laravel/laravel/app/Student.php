<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students'; // Nama tabel di database

    protected $fillable = [
        'nim', 'name', 'gender', 'departement', 'address',
    ];

    // Definisi relasi atau metode lain jika diperlukan

    // Contoh metode untuk mendapatkan data dengan kondisi tertentu
    public function scopeGender($query, $gender)
    {
        return $query->where('gender', $gender);
    }
}
